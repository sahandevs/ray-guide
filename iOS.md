#### تسک جدید
![newtask](https://gitlab.com/sahandevs/ray-guide/raw/master/task.png)

- آفلاین
- ذخیره کردن اطلاعات ثبت شده در خود دستگاه
- UI دقیقا شبیه طراحی

#### task

https://www.raywenderlich.com/34-design-patterns-by-tutorials-mvvm

https://medium.com/fantageek/rxswift-with-mvvm-e4af71413298

https://www.youtube.com/watch?v=W3zGx4TUaCE

https://github.com/ReactiveX/RxSwift/blob/master/Documentation/Examples.md#reactive-values

https://learnappmaking.com/cocoapods-playground-how-to/#playground

#### task
دوره 
https://www.youtube.com/watch?v=3Xv1mJvwXok&list=PL0dzCUj1L5JGKdVUtA5xds1zcyzsz7HLj
رو ببینید و خودتون کنارش اپی که داره میسازه رو بزنید.

#### task
![task](https://gitlab.com/sahandevs/ray-guide/raw/64843e89c5fb9e79a97b6c6eea0f185bfb3b7c2c/practice.png)

#### task
به کمک این کد: 
https://gitlab.com/sahandevs/ray-guide/blob/master/animateCell.swift
به کد قبلی انیمیشن اضافه کنید 

همینطور یک API آزمایشی ما داریم از آقای پاینده دریافت کنید و 
CollectionView
اتون رو به اون وصل کنید

---

#### task

##### resources

- [Loading](https://developer.apple.com/documentation/uikit/uiactivityindicatorview)
- [on end reached](https://stackoverflow.com/questions/48705430/swift-4-uicollectionview-detect-end-of-scrolling)

سرویس 
getList
داخل API آزمایشی یک پارامتر به نام
pageNo
میگیره که از 0 شروع میشه و یک صفحه خاص رو میاره
(داخل هر صفحه 10 آیتم هست)
حالا به اپ قبلی این قابلیت رو اضافه کنید که وقتی کاربر به آخر لیست رسید بیاد صفحه بعدی رو از سرویس بگیره و ادامه لیست قرار بده.
یک لودینگ هم وقتی در حال ارتباط با api هستیم بالا داخل 
navigationBar
دیده بشه و وقتی تموم شد hide بشه

---

#### guides

- Auto Layout Programmatically
[E1](https://www.youtube.com/watch?v=9RydRg0ZKaI) | 
[E2](https://www.youtube.com/watch?v=nv1L3mYEg8M)
- Animation
[UIView.Animated](https://www.raywenderlich.com/5255-basic-uiview-animation-tutorial-getting-started)
- [PinLayout](https://github.com/layoutBox/PinLayout)
- [UICollectionView](https://www.youtube.com/watch?v=up-YD3rZeJA)


#### resources

- [Font](https://gitlab.com/sahandevs/ray-guide/raw/master/font.ttf?inline=false)


##### resources

- https://git.ir/teamtreehouse-intermediate-swift-course/
- https://git.ir/teamtreehouse-closures-in-swift-course/