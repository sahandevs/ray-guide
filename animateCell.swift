//
//  ViewController.swift
//  testProject
//
//  Created by Sahand on 1/13/19.
//  Copyright © 2019 Sahand. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

  var dataSource: [Model] = [
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate(),
    Model.generate()
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView?.backgroundColor = .gray
    view.backgroundColor = .white
    collectionView?.register(TestCell.self, forCellWithReuseIdentifier: "test")
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "test", for: indexPath) as! TestCell
    cell.fill(isOpen: dataSource[indexPath.item].isOpen, color: dataSource[indexPath.item].color)
    return cell
  }
  
  public override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  // item click
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as! TestCell
    cell.isOpen = !cell.isOpen
    UIView.animate(withDuration: 0.2, animations: {
      cell.setNeedsLayout()
      cell.layoutIfNeeded()
      collectionView.performBatchUpdates({
        self.dataSource[indexPath.item].isOpen = !self.dataSource[indexPath.item].isOpen
      })
    })
  }
  
  let template = TestCell()
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    template.fill(isOpen: dataSource[indexPath.item].isOpen, color: dataSource[indexPath.item].color)
    return template.sizeThatFits(self.view.frame.size)
  }


}

// Cell

class TestCell: UICollectionViewCell {
  
  let bottomView = UIView()
  let coverView = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(coverView)
    addSubview(bottomView)
    bottomView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    coverView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
    self.layer.borderWidth = 2.0
    self.layer.borderColor = UIColor.red.cgColor
    self.clipsToBounds = true
  }
  
  func setup() {
    let _bottomViewHeight: CGFloat = 40
    let _coverViewHeight: CGFloat = 10
    bottomView.frame = CGRect(x: CGFloat(0), y: ( self.isOpen ? 50 : 100 ) - _bottomViewHeight, width: UIScreen.main.bounds.width, height: _bottomViewHeight)
    coverView.frame = CGRect(x: 0, y: 50, width: UIScreen.main.bounds.width, height: _coverViewHeight)
  }
  
  var isOpen: Bool = false
  func fill(isOpen: Bool, color: UIColor) {
    backgroundColor = color
    self.isOpen = isOpen
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    setup()
  }
  
  override func sizeThatFits(_ size: CGSize) -> CGSize {
    return CGSize(width: size.width, height: self.isOpen ? 50 : 100)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

// - Model

struct Model {
  
  var isOpen: Bool
  var color: UIColor
  
  // generate random Model
  static func generate() -> Model {
    return Model(isOpen: arc4random_uniform(2) == 0, color: UIColor.random)
  }
  
}

extension UIColor {
  static var random: UIColor {
    return UIColor(red: .random(in: 0...1),
                   green: .random(in: 0...1),
                   blue: .random(in: 0...1),
                   alpha: 1.0)
  }
}
